import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export function CreateRouter(){
    return new Router({
        mode: 'history',
        routes: [
            {path: '/', component: () => import('./components/HelloWorld.vue')},
            {path: '/test1', component: () => import('./components/test1.vue')},
            {path: '/test2', component: () => import('./components/test2.vue')},
        ],
    })
} 
