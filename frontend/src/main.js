import Vue from 'vue'
import App from './App.vue'
import Meta from 'vue-meta'

Vue.use(Meta)

import { CreateRouter } from './router'


Vue.config.productionTip = false

const router = CreateRouter()

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
