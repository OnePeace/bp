from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'base.html', {'Default_title': 'Кузовные запчасти в наличии и под заказ'})