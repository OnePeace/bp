import Vue from 'vue'
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/fontawesome-free-regular/shakable.es'
//Иконки по умолчанию
import {
    faUser, faLock, faSignOutAlt, faCog, faImage, faRubleSign, faQuestionCircle, faUserCircle,
    faUniversity, faChartLine, faMapMarker,
    faAngleDoubleLeft,// <<
    faAngleDoubleRight, // >>
    faBars, // |||
    faTimes, // x
    faSort, faSortUp, faSortDown, faInfo, faCheck, faAngleDown,
    faPlus, faMinus, faPlusCircle, faAngleRight,
    faFileCode, faFileExcel, faFilePdf, faFilePowerpoint, faFilter,
    faChartArea, faCogs, faTable, faEye, faDownload,
    faGraduationCap, faClipboardList
} from '@fortawesome/fontawesome-free-solid/shakable.es'

//Иконки кастомные
fontawesome.library.add(
    faUser, faLock, faSignOutAlt, faCog, faImage, faRubleSign, faQuestionCircle, faUserCircle,
    faUniversity, faChartLine, faMapMarker, faAngleDoubleLeft, faAngleDoubleRight, faBars, faTimes,
    faSort, faSortUp, faSortDown, faInfo, faCheck, faAngleDown,
    faPlus, faMinus, faPlusCircle, faAngleRight,
    faFileCode, faFileExcel, faFilePdf, faFilePowerpoint, faFilter,
    faChartArea, faCogs, faTable, faEye, faDownload,
    faGraduationCap, faClipboardList
)
