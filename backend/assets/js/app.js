import 'babel-polyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from '~/components/App'
import i18n from '~/plugins/i18n'
import store from '~/store'
import router from '~/router'

import '~/plugins'
import './components'

Vue.use(BootstrapVue)

Vue.config.productionTip = false

new Vue({
    i18n,
    store,
    router,
    ...App,
})